mod base_intersection;
mod intersection;

use intersection::mesh_plane_intersection;

use maths_rs::prelude::*;
use ndarray::{Array3, Ix3};
use numpy::{PyArray, PyArray3, PyReadonlyArray1, PyReadonlyArray2};
use pyo3::exceptions::PyValueError;
use pyo3::prelude::*;
use rayon::prelude::*;

use base_intersection::intersect_polygon_and_plane;

fn intersection_plane_conical(
    curve: &[Vec3<f64>],
    apex: Vec3<f64>,
    normal: Vec3<f64>,
    plane_point: Vec3<f64>,
) -> Vec<(Vec3<f64>, Vec3<f64>)> {
    return curve
        .par_windows(2)
        .filter_map(|points_pair| {
            let left = points_pair[0];
            let right = points_pair[1];
            let polygon = Vec::from([left, right, apex]);

            intersect_polygon_and_plane(polygon, normal, plane_point)
        })
        .collect();
}

fn array_to_vec3_vec(array: &PyReadonlyArray2<f64>) -> Vec<Vec3<f64>> {
    let view = array.as_array();
    let mut vec = Vec::with_capacity(view.shape()[0]);
    for i in 0..view.shape()[0] {
        let point = Vec3::new(view[(i, 0)], view[(i, 1)], view[(i, 2)]);
        vec.push(point);
    }

    vec
}

fn vec_f64_to_vec3_f64(v: Vec<f64>) -> Option<Vec3<f64>> {
    if v.len() == 3 {
        let arr: [f64; 3] = v.try_into().unwrap();
        Some(Vec3::new(arr[0], arr[1], arr[2]))
    } else {
        None
    }
}

fn lines_vec_to_ndarray(lines: Vec<(Vec3<f64>, Vec3<f64>)>) -> Array3<f64> {
    let mut array = Array3::<f64>::zeros([lines.len(), 2, 3]);
    for (i, (start, end)) in lines.iter().enumerate() {
        // start point
        array[[i, 0, 0]] = start.x;
        array[[i, 0, 1]] = start.y;
        array[[i, 0, 2]] = start.z;

        // end point
        array[[i, 1, 0]] = end.x;
        array[[i, 1, 1]] = end.y;
        array[[i, 1, 2]] = end.z;
    }

    array
}

#[pyfunction]
fn find_plane_and_conical_surface_intersection(
    _py: Python,
    curve: PyReadonlyArray2<f64>,
    apex: PyReadonlyArray1<f64>,
    normal: PyReadonlyArray1<f64>,
    plane_point: PyReadonlyArray1<f64>,
) -> PyResult<Py<PyArray<f64, Ix3>>> {
    let curve = array_to_vec3_vec(&curve);

    let apex = match vec_f64_to_vec3_f64(apex.to_vec().unwrap()) {
        None => return Err(PyValueError::new_err("apex length must be equal to 3")),
        Some(v) => v,
    };
    let normal = match vec_f64_to_vec3_f64(normal.to_vec().unwrap()) {
        None => return Err(PyValueError::new_err("normal length must be equal to 3")),
        Some(v) => v,
    };
    let plane_point = match vec_f64_to_vec3_f64(plane_point.to_vec().unwrap()) {
        None => {
            return Err(PyValueError::new_err(
                "plane_point length must be equal to 3",
            ))
        }
        Some(v) => v,
    };
    let vec_result = intersection_plane_conical(curve.as_slice(), apex, normal, plane_point);
    let ndarray_result = lines_vec_to_ndarray(vec_result).to_owned();

    Ok(Python::with_gil(|py| {
        let pyarray = PyArray3::from_array(py, &ndarray_result);
        pyarray.to_owned()
    }))
}

/// A Python module implemented in Rust.
#[pymodule]
fn megaplot3d_math(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(
        find_plane_and_conical_surface_intersection,
        m
    )?)?;
    m.add_function(wrap_pyfunction!(mesh_plane_intersection, m)?)?;
    Ok(())
}
