import pytest
import numpy as np

from megaplot3d_math import find_plane_and_conical_surface_intersection


def test_intersection():
    t_start = 0
    t_end = 21
    t_n = 20
    t = np.linspace(t_start, t_end, int((t_end - t_start) * t_n))
    
    x = np.cos(t) * 10
    y = np.sin(t) * 10
    z = (2 * t) + (0.7 * np.sin(t * 10))
    
    curve = np.array([x, y, z])
    curve = curve.transpose(1, 0)
    
    intersection = find_plane_and_conical_surface_intersection(
        curve=curve,
        apex=np.array([0, 0, 8], dtype=float),
        normal=np.array([1, 0.25, 0.25], dtype=float),
        plane_point=np.array([0, 0, 0], dtype=float),
    )


def main():
    intersection = test_intersection()
    
    print(f"{intersection=}")
    print(type(intersection))
    print(intersection.shape)
    
if __name__ == "__main__":
    main()