use nalgebra::Point3;
use numpy::{PyArray3, PyReadonlyArray1, PyReadonlyArray2};
use pyo3::prelude::*;
use rayon::prelude::*;

use halfspace::Halfspace;
use plane::Plane;
use points::Points;
use triangles::Triangles;

#[pyfunction]
pub fn mesh_plane_intersection(
    points: PyReadonlyArray2<f64>,
    indeces: PyReadonlyArray2<usize>,
    plane_point: PyReadonlyArray1<f64>,
    plane_normal: PyReadonlyArray1<f64>,
) -> PyResult<Py<PyArray3<f64>>> {
    let points = Points::try_from(&points)?;
    let triangles = Triangles::try_from(&indeces)?;
    let plane = Plane::from_numpy_arrays(plane_point, plane_normal)?;
    let halfspaces = calculate_halfspaces_for_points(points, plane);

    let segments: Vec<[Point3<f64>; 2]> = triangles
        .par_iter()
        .filter_map(|vertices| {
            let vertex_halfspaces = [
                halfspaces[vertices[0]],
                halfspaces[vertices[1]],
                halfspaces[vertices[2]],
            ];
            let (halfspaces, vertices) = sort_verteces_by_halfspace(vertex_halfspaces, *vertices);

            // TODO: fix spaghetti
            match halfspaces {
                [Halfspace::Positive, Halfspace::Positive, Halfspace::Positive] => None,
                [Halfspace::Plane, Halfspace::Positive, Halfspace::Positive] => None,
                [Halfspace::Plane, Halfspace::Plane, Halfspace::Positive] => {
                    Some([points[vertices[0]], points[vertices[1]]])
                }
                [Halfspace::Plane, Halfspace::Plane, Halfspace::Plane] => None,
                [Halfspace::Negative, Halfspace::Positive, Halfspace::Positive] => Some([
                    segment_plane_intersection((points[vertices[0]], points[vertices[1]]), plane),
                    segment_plane_intersection((points[vertices[0]], points[vertices[2]]), plane),
                ]),
                [Halfspace::Negative, Halfspace::Plane, Halfspace::Positive] => Some([
                    points[vertices[1]],
                    segment_plane_intersection((points[vertices[0]], points[vertices[2]]), plane),
                ]),
                [Halfspace::Negative, Halfspace::Plane, Halfspace::Plane] => {
                    Some([points[vertices[1]], points[vertices[2]]])
                }
                [Halfspace::Negative, Halfspace::Negative, Halfspace::Positive] => Some([
                    segment_plane_intersection((points[vertices[0]], points[vertices[2]]), plane),
                    segment_plane_intersection((points[vertices[1]], points[vertices[2]]), plane),
                ]),
                [Halfspace::Negative, Halfspace::Negative, Halfspace::Plane] => None,
                [Halfspace::Negative, Halfspace::Negative, Halfspace::Negative] => None,
                _ => unreachable!(),
            }
        })
        .collect();

    let segments: Vec<f64> = bytemuck::cast_vec(segments);
    let segments = ndarray::Array3::from_shape_vec((segments.len() / 6, 2, 3), segments).unwrap();
    let arr = Python::with_gil(|py| PyArray3::from_owned_array(py, segments).to_owned());
    return Ok(arr);

    fn calculate_halfspaces_for_points(points: Points, plane: Plane) -> Vec<Halfspace> {
        points
            .par_iter()
            .map(|point| Halfspace::point(point, plane))
            .collect()
    }

    fn sort_verteces_by_halfspace(
        halfspaces: [Halfspace; 3],
        indeces: [usize; 3],
    ) -> ([Halfspace; 3], [usize; 3]) {
        let mut halfspace_index_map = [
            (halfspaces[0], indeces[0]),
            (halfspaces[1], indeces[1]),
            (halfspaces[2], indeces[2]),
        ];
        halfspace_index_map.sort_by_key(|a| a.0);

        (
            [
                halfspace_index_map[0].0,
                halfspace_index_map[1].0,
                halfspace_index_map[2].0,
            ],
            [
                halfspace_index_map[0].1,
                halfspace_index_map[1].1,
                halfspace_index_map[2].1,
            ],
        )
    }

    fn segment_plane_intersection(
        segment: (Point3<f64>, Point3<f64>),
        plane: Plane,
    ) -> Point3<f64> {
        let (point_a, point_b) = (segment.0.coords, segment.1.coords);
        let t = -(plane.vector().dot(&segment.0.to_homogeneous()))
            / (plane.vector().xyz().dot(&(point_b - point_a)));

        debug_assert!((0.0..=1.0).contains(&t));

        Point3::from(point_a + t * (point_b - point_a))
    }
}

mod points {
    use nalgebra::Point3;
    use numpy::PyReadonlyArray2;

    #[derive(Debug, Clone, Copy)]
    pub(super) struct Points<'a>(&'a [Point3<f64>]);

    impl<'a, 'b> TryFrom<&'a PyReadonlyArray2<'b, f64>> for Points<'a> {
        type Error = numpy::NotContiguousError;

        fn try_from(value: &'a PyReadonlyArray2<'b, f64>) -> Result<Points<'a>, Self::Error> {
            let arr = value.as_slice()?;
            let points: &[Point3<f64>] = bytemuck::cast_slice(arr);
            Ok(Points(points))
        }
    }

    impl<'a> std::ops::Deref for Points<'a> {
        type Target = [Point3<f64>];

        fn deref(&self) -> &'a Self::Target {
            self.0
        }
    }

    impl<'a> AsRef<[Point3<f64>]> for Points<'a> {
        fn as_ref(&self) -> &[Point3<f64>] {
            self.0
        }
    }
}

mod triangles {
    use numpy::PyReadonlyArray2;

    #[derive(Debug, Clone, Copy)]
    pub(super) struct Triangles<'a>(&'a [[usize; 3]]);

    impl<'a, 'b> TryFrom<&'a PyReadonlyArray2<'b, usize>> for Triangles<'a> {
        type Error = numpy::NotContiguousError;

        fn try_from(value: &'a PyReadonlyArray2<'b, usize>) -> Result<Triangles<'a>, Self::Error> {
            let arr = value.as_slice()?;
            let points: &[[usize; 3]] = bytemuck::cast_slice(arr);
            Ok(Triangles(points))
        }
    }

    impl<'a> std::ops::Deref for Triangles<'a> {
        type Target = [[usize; 3]];

        fn deref(&self) -> &'a Self::Target {
            self.0
        }
    }

    impl<'a> AsRef<[[usize; 3]]> for Triangles<'a> {
        fn as_ref(&self) -> &[[usize; 3]] {
            self.0
        }
    }
}

mod plane {
    use nalgebra::{Point3, Vector3, Vector4};

    #[derive(Debug, Clone, Copy)]
    pub(crate) struct Plane {
        vector: Vector4<f64>,
    }

    impl Plane {
        pub(crate) fn from_numpy_arrays(
            point: numpy::PyReadonlyArray1<f64>,
            normal: numpy::PyReadonlyArray1<f64>,
        ) -> Result<Self, PlaneGenerationError> {
            let point: Point3<f64> = bytemuck::cast(
                <[f64; 3]>::try_from(
                    point
                        .as_slice()
                        .map_err(PlaneGenerationError::NotContinuous)?,
                )
                .map_err(PlaneGenerationError::TryFromSlice)?,
            );
            let normal: Vector3<f64> = bytemuck::cast(
                <[f64; 3]>::try_from(
                    normal
                        .as_slice()
                        .map_err(PlaneGenerationError::NotContinuous)?,
                )
                .map_err(PlaneGenerationError::TryFromSlice)?,
            );

            Ok(Self::new(point, normal))
        }

        pub(crate) fn new(point: Point3<f64>, normal: Vector3<f64>) -> Self {
            let d = -(point.coords.dot(&normal));
            Self {
                vector: normal.push(d),
            }
        }

        pub(crate) fn vector(&self) -> Vector4<f64> {
            self.vector
        }
    }

    pub(crate) enum PlaneGenerationError {
        NotContinuous(numpy::NotContiguousError),
        TryFromSlice(core::array::TryFromSliceError),
    }

    impl std::fmt::Display for PlaneGenerationError {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "invalid dimensions supplied for point or normal arrays")
        }
    }

    impl std::fmt::Debug for PlaneGenerationError {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "invalid dimensions supplied for point or normal arrays")
        }
    }

    impl std::error::Error for PlaneGenerationError {}

    impl From<PlaneGenerationError> for pyo3::PyErr {
        fn from(value: PlaneGenerationError) -> Self {
            pyo3::exceptions::PyValueError::new_err(value.to_string())
        }
    }
}

mod halfspace {
    use super::Plane;
    use nalgebra::Point3;

    #[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
    pub(crate) enum Halfspace {
        Positive = 1,
        Plane = 0,
        Negative = -1,
    }

    impl Halfspace {
        pub(crate) fn point(point: &Point3<f64>, plane: Plane) -> Self {
            let weight = point.to_homogeneous().dot(&plane.vector());
            match weight.total_cmp(&0.0) {
                std::cmp::Ordering::Less => Halfspace::Negative,
                std::cmp::Ordering::Equal => Halfspace::Plane,
                std::cmp::Ordering::Greater => Halfspace::Positive,
            }
        }
    }
}
