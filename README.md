# megaplot3d


## Installation

### To install our project clone repository:
```sh
git clone https://gitlab.com/megaplot3d/megaplot3d-math.git
```

### Go to project folder:
```sh
cd megaplot3d-math
```

### Make virtual environment:
```sh
python -m venv venv
```

### Activate it:

For Linux / MacOS:
```sh
. ./venv/bin/activate
```
For Windows:
```sh
.\venv\Scripts\activate
```

### Install dependencies
```
pip install -r requirements.txt
```

### And run `maturin`
```
maturin develop --release
```


## Testing
go to the project folder, activate `vanv` and run `pytest`.
```
pytest .
```
