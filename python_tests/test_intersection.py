import pytest
import pyvista as pv
import numpy as np

from megaplot3d_math import mesh_plane_intersection


def build_conical_surf(x, y, z, apex, subdivide_number: int = 10):
    num_points = subdivide_number + 1
    points = np.zeros((num_points + 1, len(x), 3))
    for i in range(num_points + 1):
        points[i, :, :] = (1 - i / num_points) * apex + (
            i / num_points
        ) * np.column_stack((x, y, z))

    return points


def get_polygons_from_mesh(mesh: pv.PolyData) -> np.ndarray:
    print(mesh)
    return mesh.delaunay_3d().extract_geometry()
    # faces = mesh.faces.reshape((-1, 4))
    # faces = faces[:, 1:]


def test_intersection():
    t_start = 0
    t_end = 21
    t_n = 20
    t = np.linspace(t_start, t_end, int((t_end - t_start) * t_n))

    x = np.cos(t) * 10
    y = np.sin(t) * 10
    z = (2 * t) + (0.7 * np.sin(t * 10))

    curve = np.array([x, y, z])
    curve = curve.transpose(1, 0)

    x, y, z = curve.transpose(1, 0)

    points = build_conical_surf(x, y, z, np.array([0, 0, 8], dtype=float), 1)
    surface = pv.StructuredGrid(
        points[:, :, 0],
        points[:, :, 1],
        points[:, :, 2],
    )

    # faces = surface.triangulate()
    # print(type(faces))
    mesh = surface.delaunay_3d().extract_geometry()
    print(type(mesh))
    faces = mesh.faces
    print(type(faces))
    points = mesh.points
    print(type(points))
    # points = faces.points
    # indices, cell_types = faces.get_indexed_cells()
    # print(cell_types)
    # faces = get_polygons_from_mesh(surface)
    # points = surface.points()

    # faces = mesh.faces.reshape((-1, 4))
    indeces = faces.reshape((-1, 4))[:, 1::]
    print(indeces.shape)
    print(indeces)

    return mesh_plane_intersection(
        points,
        indeces.astype(np.uint64),
        np.array([0.0, 0.0, 0.0]),
        np.array([1.0, 0.25, 0.25]),
    )
    return None



def main():
    intersection = test_intersection()

    print(f"{intersection=}")
    print(type(intersection))
    print(intersection.shape)

if __name__ == "__main__":
    main()
