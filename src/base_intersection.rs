use maths_rs::prelude::*;

const EPS: f64 = 0.001;

fn intersection_segment_plane(
    left: Vec3<f64>,
    right: Vec3<f64>,
    normal: Vec3<f64>,
    point: Vec3<f64>,
) -> Option<Vec3<f64>> {
    let d = -(dot(point, normal));
    let plane = Vec4::<f64>::from((normal, d));

    let t = -(dot(plane, Vec4::from((left, 1.0)))) / (dot(plane.xyz(), right - left));

    if (0.0..=1.0).contains(&t) {
        Some(left + t * (right - left))
    } else {
        None
    }
}

fn compare_points(first: Vec3<f64>, second: Vec3<f64>, eps: f64) -> bool {
    (dist(first, second)) <= eps
}

pub fn intersect_polygon_and_plane(
    polygon: Vec<Vec3<f64>>,
    normal: Vec3<f64>,
    plane_point: Vec3<f64>,
) -> Option<(Vec3<f64>, Vec3<f64>)> {
    let mut a = intersection_segment_plane(polygon[0], polygon[1], normal, plane_point);
    let mut b = intersection_segment_plane(polygon[0], polygon[2], normal, plane_point);
    let mut c = intersection_segment_plane(polygon[1], polygon[2], normal, plane_point);

    if let [None, None, None] = [a, b, c] {
        return None;
    }

    if let [Some(first), Some(second), Some(third)] = [a, b, c] {
        if compare_points(first, second, EPS) {
            b = None;
        } else if compare_points(first, third, EPS) {
            a = None;
        } else if compare_points(second, third, EPS) {
            c = None;
        } else {
            return None;
        }
    }

    if a.is_none() {
        let [b, c] = [b.unwrap(), c.unwrap()];
        if !compare_points(b, c, EPS) {
            return Some((b, c));
            // lines.push((b, c));
        }
    }

    if b.is_none() {
        let [a, c] = [a.unwrap(), c.unwrap()];
        if !compare_points(a, c, EPS) {
            return Some((a, c));
            // lines.push((a, c));
        }
    }

    if c.is_none() {
        let [a, b] = [a.unwrap(), b.unwrap()];
        if !compare_points(a, b, EPS) {
            return Some((a, b));
            // lines.push((a, b));
        }
    }

    None
}
